import {BrowserModule} from '@angular/platform-browser'
import {NgModule} from '@angular/core'

import {AppComponent} from './app.component'
import {FieldComponent} from './component/field/field.component'
import {CellComponent} from './component/cell/cell.component'
import {FormsModule} from '@angular/forms'
import {NumberPadComponent} from './component/number-pad/number-pad.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

@NgModule({
  declarations: [
    AppComponent,
    FieldComponent,
    CellComponent,
    NumberPadComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
