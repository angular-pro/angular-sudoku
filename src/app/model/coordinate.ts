import {size} from './constants'

export class Coordinate {
    constructor(readonly row, readonly col) {
        if (this.col >= size) {
            this.row++
            this.col -= size
        }
        if (this.col < 0) {
            this.row--
            this.col += size
        }
    }

}
