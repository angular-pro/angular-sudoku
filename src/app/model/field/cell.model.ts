import {SudokuCell} from 'sudoku-dlx/built/typings/lib'

export class Cell implements SudokuCell {
  wrong: boolean
  disabled: boolean
  number: number
  note: number

  constructor(public row: number, public col: number) {

  }
}
