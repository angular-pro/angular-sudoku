import {Field} from '../field.model'
import {solveCells, SudokuCell} from 'sudoku-dlx'
import {LinkedList} from 'linked-list-typescript'
import {Cell} from '../cell.model'

export class Puzzler {

  constructor(private readonly field: Field) {
  }

  private static randomIndex(length: number): number {
    return Math.floor(Math.random() * length)
  }

  public puzzle(): void {
    const sudokuCells = this.convertToSudokuCells(this.field)
    let sudokuCellsArray = sudokuCells.toArray()
    let cell: Cell
    do {
      const index = Puzzler.randomIndex(sudokuCells.length)
      cell = sudokuCellsArray[index]
      sudokuCells.remove(cell)
      sudokuCellsArray = sudokuCells.toArray()
    } while (this.numberOfSolutions(sudokuCellsArray) === 1)
    sudokuCells.append(cell)
    this.convertToField(sudokuCells)
  }

  convertToField(sudokuCells: LinkedList<Cell>): void {
    this.field.clear()
    for (const sudokuCell of sudokuCells) {
      this.field[sudokuCell.row][sudokuCell.col].number = sudokuCell.number
      this.field[sudokuCell.row][sudokuCell.col].disabled = true
    }
  }

  numberOfSolutions(sudokuCells: SudokuCell[]): number {
    const solvedCells = solveCells(sudokuCells, true)
    return solvedCells.length
  }

  private convertToSudokuCells(field: Field): LinkedList<Cell> {
    const sudokuCells: LinkedList<Cell> = new LinkedList()
    field.forEach(rowCells =>
      rowCells.forEach(value => {
        if (value.number) {
          sudokuCells.append({...value})
        }
      }))
    return sudokuCells
  }
}
