import {size} from '../constants'
import {Cell} from './cell.model'

export class Field extends Array<Array<Cell>> {
  fulled: Array<Array<Cell>>

  constructor() {
    super(size)
    for (let i = 0; i < size; i++) {
      this[i] = new Array<Cell>()
      for (let j = 0; j < size; j++) {
        this[i].push(new Cell(i, j))
      }
    }
  }

  check(): number {
    let isNotFull = 0
    for (let i = 0; i < size; i++) {
      for (let j = 0; j < size; j++) {
        const errorType = this.checkI(i, j)
        if (errorType > 0) {
          return errorType
        } else if (errorType < 0) {
          isNotFull = errorType
        }
      }
    }
    return isNotFull
  }

  checkI(rowNumber: number, colNumber: number): number {
    let errorType = this.checkLine(this.row(rowNumber))
    let isNotFull = 0
    if (errorType > 0) {
      return 1
    } else if (errorType < 0) {
      isNotFull = errorType
    }
    const col = this.col(colNumber)
    errorType = this.checkLine(col)
    if (errorType > 0) {
      return 2
    } else if (errorType < 0) {
      isNotFull = errorType
    }
    const square = this.square(rowNumber, colNumber)
    errorType = this.checkLine(square)
    if (errorType > 0) {
      return 3
    } else if (errorType < 0) {
      isNotFull = errorType
    }
    return isNotFull
  }

  private checkLine(line: Array<number>): number {
    if (line.some((value, index) =>
      value != null && index !== line.lastIndexOf(value))) {
      return 1
    }
    if (line.some(value => value == null)) {
      return -1
    }
    return 0
  }

  row(row: number): number[] {
    return this[row].map(value => value.number)
  }

  col(i: number): number[] {
    const col = []
    for (let j = 0; j < 9; j++) {
      col.push(this[j][i].number)
    }
    return col
  }


  square(rowNumber: number, colNumber: number): number[] {
    const square = []
    const baseRowNumber = 3 * Math.trunc(rowNumber / 3)
    const baseColNumber = 3 * Math.trunc(colNumber / 3)
    for (let j = baseRowNumber; j < 3 + baseRowNumber; j++) {
      for (let k = baseColNumber; k < 3 + baseColNumber; k++) {
        square.push(this[j][k].number)
      }
    }
    return square

  }

  clear(): void {
    this.forEach(value => value.forEach((value1, index, array) =>
      array[index].number = null))
  }
}

