import {Coordinate} from '../../coordinate'
import {Field} from '../field.model'
import {size} from '../../constants'
import {Cell} from '../cell.model'

export class FieldGenerator {
  private fullLine = []

  private impossibleValues: Array<Array<Array<number>>> = new Array<Array<Array<number>>>()

  constructor(private field: Field) {
    for (let i = 0; i < size; i++) {
      this.impossibleValues.push([])
      for (let j = 0; j < size; j++) {
        this.impossibleValues[i].push([])
      }
      this.fullLine.push(i + 1)
    }
  }


  private static randomIndex(length: number): number {
    return Math.floor(Math.random() * length)
  }

  public fillField(): void {
    this.fillCellRecursive(new Coordinate(0, 0))

    this.field.fulled = []
    for (let i = 0; i < size; i++) {
      this.field.fulled.push(this.field[i].map(value => {return {...value}}))
    }
  }

  private fillCellRecursive({row, col}: Coordinate): void {
    if (row >= size) {
      return
    }
    const impossibleNumbers: number[] = this.impossibleValues[row][col]
    const possibleNumbers = this.filterPossibleNumbers({row, col}, impossibleNumbers)
    if (possibleNumbers.length === 0) {
      this.field[row][col].number = null
      impossibleNumbers.splice(0)
      this.fillCellRecursive(new Coordinate(row, col - 1))
    } else {
      const index: number = FieldGenerator.randomIndex(possibleNumbers.length)
      const num: number = possibleNumbers[index]
      this.field[row][col].number = num
      impossibleNumbers.push(num)
      this.fillCellRecursive(new Coordinate(row, col + 1))
    }
  }

  private filterPossibleNumbers({row, col}: Coordinate, impossibleNumbers: number[]): number[] {
    return [...this.fullLine]
      .filter(value => !this.field.row(row).includes(value))
      .filter(value => !this.field.col(col).includes(value))
      .filter(value => !this.field.square(row, col).includes(value))
      .filter(value => !impossibleNumbers.includes(value))
  }
}
