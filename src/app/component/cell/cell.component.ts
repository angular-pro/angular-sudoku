import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core'
import {Cell} from '../../model/field/cell.model'

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent implements OnInit, OnChanges {

  @ViewChild('input') input: ElementRef<HTMLInputElement>
  @Input()
  row: number

  @Input()
  col: number

  @Input()
  cell: Cell

  @Input()
  disabled: boolean

  @Input()
  win: boolean

  @Output()
  changed: EventEmitter<number> = new EventEmitter<number>()

  constructor() {
  }

  ngOnInit(): void {

  }

  focus(): void {
    const nativeElement: HTMLInputElement = this.input.nativeElement
    nativeElement.focus()
    nativeElement.select()
  }

  select() {
    const nativeElement: HTMLInputElement = this.input.nativeElement
    nativeElement.select()
  }

  click() {

  }

  selectNumber(value: number) {
    this.cell.number = value
    this.select()
    this.changed.emit(value)
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.changed.emit(this.cell.number)
  }

  selectNote(value: number) {
      this.cell.note = value
      this.select()
  }

}
