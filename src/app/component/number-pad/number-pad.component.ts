import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core'
import {size} from '../../model/constants'

@Component({
  selector: 'app-number-pad',
  templateUrl: './number-pad.component.html',
  styleUrls: ['./number-pad.component.css']
})
export class NumberPadComponent implements OnInit {
  size: number = size

  @Output()
  selectedNumber: EventEmitter<number> = new EventEmitter<number>()
  @Output()
  selectedNote: EventEmitter<number> = new EventEmitter<number>()

  cols(row): number[] {
    const slice = [...Array(size * row / 3 + 1).keys()].slice(size * (row - 1) / 3 + 1)
    return slice
  }

  constructor() {
  }

  ngOnInit(): void {
  }

  select(number: number): void {
    this.selectedNumber.emit(number)
  }

  makeNote(number: number) {
    this.selectedNote.emit(number)
    return false
  }
}
