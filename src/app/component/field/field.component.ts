import {Component, OnInit, QueryList, ViewChildren} from '@angular/core'
import {Field} from '../../model/field/field.model'
import {size} from '../../model/constants'
import {FieldGenerator} from '../../model/field/generator/field-generator.model'
import {Puzzler} from '../../model/field/puzzler/puzzler.model'
import {CellComponent} from '../cell/cell.component'

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.css']
})
export class FieldComponent implements OnInit {

  @ViewChildren(CellComponent) cellComponentChildren: QueryList<CellComponent>
  win: boolean
  disabled: boolean

  constructor() {
  }

  sizeArray = []
  field = new Field()
  showField = false

  ngOnInit(): void {
    for (let i = 0; i < size; i++) {
      this.sizeArray.push(i)
    }
    new FieldGenerator(this.field).fillField()
    new Puzzler(this.field).puzzle()
  }

  setValue(event: number, row: number, col: number): void {
    const val = Number(event)
    const cell = this.field[row][col]
    if (val && val > 0 && val <= size) {
      cell.number = val
      const check = this.field.check()
      // cell.wrong = check > 0
      if (check === 0) {
        this.win = true
        this.disabled = true
      }
    } else {
      cell.number = null
    }
    // this.navigate(null, row, col, 0, 0)
  }


  navigate($event: Event, rowIndex: number, colIndex: number, rowDirection, colDirection): void {
    const newRow = rowIndex + rowDirection
    const newCol = colIndex + colDirection
    if (newRow >= 0 && newRow < size && newCol >= 0 && newCol < size) {
      const cell = this.cellComponentChildren.find(item => item.row === newRow && item.col === newCol)
      if (cell.disabled) {
        this.navigate($event, newRow, newCol, rowDirection, colDirection)
      } else {
        cell.focus()
      }
    }
  }

  prevent($event: KeyboardEvent): void {
    $event.preventDefault()
  }

}
